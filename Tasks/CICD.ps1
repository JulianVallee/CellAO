$BaseDir = "c:\Src\CellAO\"
Set-Location $BaseDir
powershell.exe -File "Tasks\GetLatest.ps1"
powershell.exe -File "Tasks\Build.ps1" -BasePath $BaseDir

xcopy c:\Src\CellAO\CellAO\Built\Debug\*.* c:\Hosting\Phoenix\*.* /s /y

Stop-Process -ProcessName ZoneEngine
Stop-Process -ProcessName LoginEngine
Stop-Process -ProcessName ChatEngine

Set-Location "c:\hosting\Phoenix"

Start-Process -FilePath ZoneEngine.exe
Start-Process -FilePath LoginEngine.exe
Start-Process -FilePath ChatEngine.exe
