﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CellAO.Studio.Extensions
{
    using System.Reflection;
    using System;
    using System.Text;
    using CellAO.Messages;

    using Utility;

    
    using Message = CellAO.Messages.Message;


    public static class ByteArrayExtensions
    {
        public static string ToString(this byte[] data)
        {
            return HexOutput.Output(data);
        }
    }

    public static class MessageExtension
    {
        private const int columnWidth = 25;

        public static String Name(this N3MessageType t)
        {
            return t.ToString();
        }

        public static string DebugStringComplete(this Message message)
        {
            if (message != null && message.Body != null && message.Body is N3Message)
            {
                var body = message.Body as N3Message;
                return "Header: " + Environment.NewLine + message.Header.DebugHeader() + Environment.NewLine
                       + body.DebugMessage() + Environment.NewLine + Environment.NewLine;
            }
            else if (message != null && message.Body != null)
            {
                return message.Body.ToString();
            }
            return "";
        }

        public static string DebugHeader(this Header header)
        {
            StringBuilder sb = new StringBuilder();
            sb.AppendLine("PacketId".PadRight(25) + header.MessageId.ToString("X"));
            sb.AppendLine(
                "Packet Type".PadRight(25) + header.PacketType.ToString() + " ("
                + ((short)header.PacketType).ToString("X4") + ")");
            sb.AppendLine("Receiver".PadRight(25) + ((int)header.Receiver).ToString("X"));
            sb.AppendLine("Sender".PadRight(25) + ((int)header.Sender).ToString("X8"));
            sb.AppendLine("Size".PadRight(25) + header.Size.ToString() + " (" + ((int)header.Size).ToString("X4") + ")");
            sb.AppendLine("Unknown".PadRight(25) + header.Unknown.ToString("X"));
            return sb.ToString();
        }

        public static string DebugMessage(this N3Message msg)
        {
            if (msg == null)
            {
                return "Body is null, message was not deserialized.";
            }
            return msg.DebugString();
        }
        
    }
}
