﻿#region License

// Copyright (c) 2005-2016, CellAO Team
// 
// 
// All rights reserved.
// 
// 
// Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:
// 
// 
//     * Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.
//     * Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in the documentation and/or other materials provided with the distribution.
//     * Neither the name of the CellAO Team nor the names of its contributors may be used to endorse or promote products derived from this software without specific prior written permission.
// 
// 
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
// "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
// LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
// A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR
// CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
// EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
// PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
// PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
// LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
// NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
// SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
// 

#endregion

namespace ChatEngine.Relay
{
    #region Usings ...

    using System.Data.SqlClient;
    using System.Diagnostics;

    using CellAO.Database.Dao;

    using IrcDotNet;
    using AO.Core.Encryption;
    using System;
    using CellAO.Database.Entities;

    #endregion

    /// <summary>
    /// </summary>
    public class CellAoBotUser
    {
        #region Constructors and Destructors

        /// <summary>
        /// </summary>
        /// <param name="ircUser">
        /// </param>
        public CellAoBotUser(IrcUser ircUser)
        {
            Debug.Assert(ircUser != null);
            this.IrcUser = ircUser;
        }
        public CellAoBotUser()
        {

        }

        #endregion

        #region Public Properties

        /// <summary>
        /// </summary>
        public IrcUser IrcUser { get; private set; }

        /// <summary>
        /// </summary>
        public bool IsAuthenticated { get; private set; }

        /// <summary>
        /// 
        /// </summary>
        public int GMlevel { get; private set; }

        /// <summary>
        /// 
        /// </summary>
     //   public bool IsRegistered { get; private set; }

        #endregion

        #region Public Methods and Operators

        /// <summary>
        /// </summary>
        /// <param name="username">
        /// </param>
        /// <param name="password">
        /// </param>
        /// <returns>
        /// </returns>
        public void LogIn(string username, string password)
        {
            try
            {
                string dUser = LoginDataDao.Instance.GetByUsername(username).Username;
                string dPass = LoginDataDao.Instance.GetByUsername(username).Password;

                if (dUser != username)
                {
                    this.IsAuthenticated = false;
                }

                this.IsAuthenticated = true;
            }
            catch (SqlException)
            {
                throw;
            }
        }

        public int GMLevel(string username)
        {
            this.GMlevel = LoginDataDao.Instance.GetByUsername(username).GM;
            return this.GMlevel;
        }


        public bool loggedin(string username, string password)
        {
            LoginEncryption LE = new LoginEncryption();
            string dUser = LoginDataDao.Instance.GetByUsername(username).Username;
            string dPass = LoginDataDao.Instance.GetByUsername(username).Password;
            if (dUser != username && dPass != LE.GeneratePasswordHash(dPass))
            {
                return false;
            }
            else
            {
                return true;
            }
        }

        /// <summary>
        /// </summary>
        public void LogOut()
        {
            this.IsAuthenticated = false;
            this.GMlevel = 0;
        }

        public bool Register(string _Username, string _Password, string _Firstname, string _Lastname, string _Email)
        {

            if (LoginDataDao.Instance.Exists(_Username))
            {
                return false;
            }
            else
            {

                DBLogin login = new DBLogin
                {
                    Username = _Username,
                    AccountFlags = 0,
                    AllowedCharacters = 12,
                    CreationDate = DateTime.Now,
                    Email = _Email,
                    Expansions = 2047,
                    FirstName = _Firstname,
                    LastName = _Lastname,
                    GM = 0,
                    Flags = 0,
                    Password = new LoginEncryption().GeneratePasswordHash(_Password)
                };

                try
                {
                    LoginDataDao.WriteLoginData(login);
                    return true;
                }
                catch(SqlException)
                {
                    return false;
                }
            }


        }
        #endregion
    }
}