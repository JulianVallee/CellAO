﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="CorpseFullUpdateSerializer.cs" company="SmokeLounge">
//   Copyright © 2013 SmokeLounge.
//   This program is free software. It comes without any warranty, to
//   the extent permitted by applicable law. You can redistribute it
//   and/or modify it under the terms of the Do What The Fuck You Want
//   To Public License, Version 2, as published by Sam Hocevar. See
//   http://www.wtfpl.net/ for more details.
// </copyright>
// <summary>
//   Defines the CorpseFullUpdateSerializer type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace CellAO.Messages.Messages.Serialization.Serializers.Custom
{
    using System;
    using System.Collections.Generic;
    using System.Linq.Expressions;
    using System.Text;

    using CellAO.Enums;
    using CellAO.Messages.N3Messages;
    using CellAO.Interfaces;
    using CellAO.Messages.Fields;

    public class CorpseFullUpdateSerializer : ISerializer
    {
        public CorpseFullUpdateSerializer()
        {
            this.Type = typeof(CorpseFullUpdateMessage);
        }

        public Type Type { get; }

        #region Public Methods and Operators

        public object Deserialize(StreamReader streamReader, SerializationContext serializationContext, PropertyMetaData propertyMetaData = null)
        {
            var corpseMsg = new CorpseFullUpdateMessage();

            corpseMsg.N3MessageType = (N3MessageType)streamReader.ReadInt32();
            corpseMsg.Identity = streamReader.ReadIdentity();
            corpseMsg.Unknown = streamReader.ReadByte();
            corpseMsg.CfuMsgVersion = streamReader.ReadInt32();
            corpseMsg.SifuMsgVersion = streamReader.ReadInt32();
            corpseMsg.Owner = streamReader.ReadIdentity();
            corpseMsg.Position = new Vector3(streamReader.ReadSingle(), streamReader.ReadSingle(), streamReader.ReadSingle());
            corpseMsg.Rotation = new Quaternion
            {
                X = streamReader.ReadSingle(),
                Y = streamReader.ReadSingle(),
                Z = streamReader.ReadSingle(),
                W = streamReader.ReadSingle()
            };

            corpseMsg.Playfield = streamReader.ReadInt32();
            corpseMsg.Unknown1 = streamReader.ReadIdentity();
            corpseMsg.Unknown2 = streamReader.ReadByte();
            corpseMsg.Unknown3 = streamReader.ReadByte();

            var statCount = (streamReader.ReadInt32() / 1009) - 1;
            var stats = new List<GameTuple<StatIds, uint>>();

            for (var i = 0; i < statCount; i++)
            {
                var value1 = (StatIds)streamReader.ReadInt32();
                var value2 = streamReader.ReadUInt32();
                stats.Add(new GameTuple<StatIds, uint> { Value1 = value1, Value2 = value2 });
            }

            corpseMsg.Stats = stats.ToArray();

            var nameLength = streamReader.ReadInt32();
            corpseMsg.Name = streamReader.ReadString(nameLength);
            corpseMsg.LifuMsgVersion = streamReader.ReadInt32();
            corpseMsg.Unknown4 = streamReader.ReadInt32();

            var identityCount = (streamReader.ReadInt32() / 1009) - 1;
            var identites = new List<Identity>();

            for (var i = 0; i < identityCount; i++)
            {
                var identity = streamReader.ReadIdentity();

                identites.Add(identity);
            }

            corpseMsg.Identities = identites.ToArray();

            corpseMsg.CifuMsgVersion = streamReader.ReadInt32();

            if (!streamReader.EndOfStream())
            {
                var spellDataCount = (streamReader.ReadInt32() / 1009) - 1;
                var spellData = new List<ItemAnimEffect>();

                for (var i = 0; i < spellDataCount; i++)
                {

                    var unknown1 = streamReader.ReadInt32();
                    var nanoId = streamReader.ReadInt32();
                    var nanoInstance = streamReader.ReadInt32();
                    var time1 = streamReader.ReadInt32();
                    var time2 = streamReader.ReadInt32();
                    var unk2 = streamReader.ReadInt32();
                    var unk3 = streamReader.ReadInt32();
                    var unk4 = streamReader.ReadInt32();
                    var unk5 = streamReader.ReadInt32();
                    var unk6 = streamReader.ReadInt32();
                    var unk7 = streamReader.ReadInt32();
                    var unk8 = streamReader.ReadInt32();
                    var unk9 = streamReader.ReadInt32();
                    var unk10 = streamReader.ReadInt32();
                    var unk11 = streamReader.ReadInt32();

                    spellData.Add(new ItemAnimEffect()
                    {
                        Unknown1 = unknown1,
                        NanoId = nanoId,
                        NanoInstance = nanoInstance,
                        Time1 = time1,
                        Time2 = time2,
                        Unknown2 = unk2,
                        Unknown3 = unk3,
                        Unknown4 = unk4,
                        Unknown5 = unk5,
                        Unknown6 = unk6,
                        Unknown7 = unk7,
                        Unknown8 = unk8,
                        Unknown9 = unk9,
                        Unknown10 = unk10,
                        Unknown11 = unk11,
                    });
                }

                corpseMsg.SpellData = spellData.ToArray();

                corpseMsg.Unknown5 = new Identity { Type = (IdentityType)streamReader.ReadInt32(), Instance = streamReader.ReadInt32() };

                var clothDathCount = (streamReader.ReadInt32() / 1009) - 1;
                var clothData = new List<Cloth>();

                for (var i = 0; i < clothDathCount; i++)
                {
                    var id = streamReader.ReadInt32();
                    var unk2 = streamReader.ReadInt32();
                    var unk3 = streamReader.ReadInt32();

                    clothData.Add(new Cloth() { ID = id, Unknown2 = unk2, Unknown3 = unk3 });
                }

                corpseMsg.ClothData = clothData.ToArray();

                var hasTextureData = streamReader.ReadInt32();

                if (hasTextureData == 1)
                {
                    var textureCount = (streamReader.ReadInt32() / 1009) - 1;
                    var textureData = new List<Texture>();

                    for (var i = 0; i < textureCount; i++)
                    {
                        var name = Encoding.Default.GetString(streamReader.ReadBytes(32));
                        var place = streamReader.ReadInt32();
                        var id = streamReader.ReadInt32();
                        var unknown = streamReader.ReadInt32();

                        textureData.Add(new Texture() { Name = name, Place = place, Id = id, Unknown = unknown });
                    }

                    corpseMsg.Textures = textureData.ToArray();
                }
            }

            return corpseMsg;
        }

        public Expression DeserializerExpression(ParameterExpression streamReaderExpression, ParameterExpression serializationContextExpression, Expression assignmentTargetExpression, PropertyMetaData propertyMetaData)
        {

            var deserializerMethodInfo =
                ReflectionHelper
                    .GetMethodInfo
                    <CorpseFullUpdateSerializer, Func<StreamReader, SerializationContext, PropertyMetaData, object>>
                    (o => o.Deserialize);
            var serializerExp = Expression.New(this.GetType());
            var callExp = Expression.Call(
                serializerExp,
                deserializerMethodInfo,
                new Expression[]
                    {
                        streamReaderExpression, serializationContextExpression,
                        Expression.Constant(propertyMetaData, typeof(PropertyMetaData))
                    });

            var assignmentExp = Expression.Assign(
                assignmentTargetExpression, Expression.TypeAs(callExp, assignmentTargetExpression.Type));
            return assignmentExp;
        }

        public void Serialize(
            StreamWriter streamWriter,
            SerializationContext serializationContext,
            object value,
            PropertyMetaData propertyMetaData = null)
        {
            if (!(value is CorpseFullUpdateMessage)) return;

            var corpseMsg = (CorpseFullUpdateMessage)value;

            streamWriter.WriteInt32((int)corpseMsg.N3MessageType);
            streamWriter.WriteIdentity(corpseMsg.Identity);
            streamWriter.WriteByte(corpseMsg.Unknown);
            streamWriter.WriteInt32(corpseMsg.CfuMsgVersion);
            streamWriter.WriteInt32(corpseMsg.SifuMsgVersion);
            streamWriter.WriteIdentity(corpseMsg.Owner);
            streamWriter.WriteSingle(corpseMsg.Position.X);
            streamWriter.WriteSingle(corpseMsg.Position.Y);
            streamWriter.WriteSingle(corpseMsg.Position.Z);
            streamWriter.WriteSingle(corpseMsg.Rotation.X);
            streamWriter.WriteSingle(corpseMsg.Rotation.Y);
            streamWriter.WriteSingle(corpseMsg.Rotation.Z);
            streamWriter.WriteSingle(corpseMsg.Rotation.W);
            streamWriter.WriteInt32(corpseMsg.Playfield);
            streamWriter.WriteIdentity(corpseMsg.Unknown1);
            streamWriter.WriteByte(corpseMsg.Unknown2);
            streamWriter.WriteByte(corpseMsg.Unknown3);

            streamWriter.WriteInt32((corpseMsg.Stats.Length + 1) * 1009);

            foreach (var stat in corpseMsg.Stats)
            {
                streamWriter.WriteInt32((int)stat.Value1);
                streamWriter.WriteUInt32(stat.Value2);
            }

            streamWriter.WriteInt32(corpseMsg.Name.Length + 1);

            streamWriter.WriteString(corpseMsg.Name);
            streamWriter.WriteByte(0);

            streamWriter.WriteInt32(corpseMsg.LifuMsgVersion);
            streamWriter.WriteInt32(corpseMsg.Unknown4);

            streamWriter.WriteInt32((corpseMsg.Identities.Length + 1) * 1009);
            foreach (var corpseMsgIdentity in corpseMsg.Identities)
            {
                streamWriter.WriteIdentity(corpseMsgIdentity);
            }

            streamWriter.WriteInt32(corpseMsg.CifuMsgVersion);

            var spellDataCount = (corpseMsg.SpellData.Length + 1) * 1009;

            streamWriter.WriteInt32(spellDataCount);

            if (spellDataCount > 1009)
            { // might be wrong to check this, but seems like they place som unknow spell just to write the rest


                foreach (var activeNano in corpseMsg.SpellData)
                {
                    streamWriter.WriteInt32(activeNano.Unknown1);
                    streamWriter.WriteInt32(activeNano.NanoId);
                    streamWriter.WriteInt32(activeNano.NanoInstance);
                    streamWriter.WriteInt32(activeNano.Time1);
                    streamWriter.WriteInt32(activeNano.Time2);

                    streamWriter.WriteInt32(activeNano.Unknown2);
                    streamWriter.WriteInt32(activeNano.Unknown3);
                    streamWriter.WriteInt32(activeNano.Unknown4);
                    streamWriter.WriteInt32(activeNano.Unknown5);
                    streamWriter.WriteInt32(activeNano.Unknown6);
                    streamWriter.WriteInt32(activeNano.Unknown7);
                    streamWriter.WriteInt32(activeNano.Unknown8);
                    streamWriter.WriteInt32(activeNano.Unknown9);
                    streamWriter.WriteInt32(activeNano.Unknown10);
                    streamWriter.WriteInt32(activeNano.Unknown11);
                }
            }

            streamWriter.WriteIdentity(corpseMsg.Unknown5);
            streamWriter.WriteInt32((corpseMsg.ClothData.Length + 1) * 1009);
            foreach (var cloth in corpseMsg.ClothData)
            {
                streamWriter.WriteInt32(cloth.ID);
                streamWriter.WriteInt32(cloth.Unknown2);
                streamWriter.WriteInt32(cloth.Unknown3);
            }

            if (corpseMsg.Textures != null && corpseMsg.Textures.Length > 0)
            {
                streamWriter.WriteInt32(0x1);

                streamWriter.WriteInt32((corpseMsg.Textures.Length + 1) * 1009);
                foreach (var corpseMsgTexture in corpseMsg.Textures)
                {
                    var name = Encoding.ASCII.GetBytes(corpseMsgTexture.Name);
                    streamWriter.WriteBytes(name);
                    streamWriter.WriteInt32(corpseMsgTexture.Place);
                    streamWriter.WriteInt32(corpseMsgTexture.Id);
                    streamWriter.WriteInt32(corpseMsgTexture.Unknown);
                }
            }
            else
            {
                streamWriter.WriteInt32(0x0);
            }
        }

        public Expression SerializerExpression(
            ParameterExpression streamWriterExpression,
            ParameterExpression serializationContextExpression,
            Expression valueExpression,
            PropertyMetaData propertyMetaData)
        {
            var serializerMethodInfo =
                ReflectionHelper
                    .GetMethodInfo
                    <CorpseFullUpdateSerializer,
                        Action<StreamWriter, SerializationContext, object, PropertyMetaData>>(o => o.Serialize);
            var serializerExp = Expression.New(this.GetType());
            var callExp = Expression.Call(
                serializerExp,
                serializerMethodInfo,
                new[]
                    {
                        streamWriterExpression, serializationContextExpression, valueExpression,
                        Expression.Constant(propertyMetaData, typeof(PropertyMetaData))
                    });
            return callExp;
        }

        #endregion
    }
}