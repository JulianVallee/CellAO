﻿#region License

// Copyright (c) 2005-2014, CellAO Team
// 
// 
// All rights reserved.
// 
// 
// Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:
// 
// 
//     * Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.
//     * Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in the documentation and/or other materials provided with the distribution.
//     * Neither the name of the CellAO Team nor the names of its contributors may be used to endorse or promote products derived from this software without specific prior written permission.
// 
// 
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
// "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
// LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
// A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR
// CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
// EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
// PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
// PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
// LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
// NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
// SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
// 

#endregion

namespace AOInject
{
    #region Usings ...

    using System;
    using System.Collections.Generic;
    using System.Runtime.InteropServices;
    using System.Threading;

    using EasyHook;
    #endregion

    /// <summary>
    /// </summary>
    public class Main : IEntryPoint
    {
        #region Static Fields

        /// <summary>
        /// </summary>
        private static DDataBlockToMessage dataBlockToMessageHolder;

        private static DSend sendHolder;

        #endregion

        #region Fields

        /// <summary>
        /// </summary>
        private readonly HookInterface Interface;

        /// <summary>
        /// </summary>
        private readonly Stack<DataClass> incomingQueue = new Stack<DataClass>();

        private readonly Stack<DataClass> outgoingQueue = new Stack<DataClass>();

        /// <summary>
        /// </summary>
        private LocalHook receiveMessageHook;

        private LocalHook sendMessageHook;

        #endregion

        #region Constructors and Destructors

        /// <summary>
        /// </summary>
        /// <param name="inContext">
        /// </param>
        /// <param name="inChannelName">
        /// </param>
        public Main(RemoteHooking.IContext inContext, string inChannelName)
        {
            Interface = RemoteHooking.IpcConnectClient<HookInterface>(inChannelName);
            Interface.Ping();
        }

        #endregion

        #region Delegates

        [UnmanagedFunctionPointer(CallingConvention.StdCall, CharSet = CharSet.Unicode)]
        private delegate int DSend(
            [MarshalAs(UnmanagedType.U4)] uint s,
            [In] [MarshalAs(UnmanagedType.LPArray, SizeParamIndex = 2)] byte[] buf,
            int len,
            int msgFlags);

        /// <summary>
        /// </summary>
        /// <param name="_this">
        /// </param>
        /// <param name="size">
        /// </param>
        /// <param name="dataBlock">
        /// </param>
        [UnmanagedFunctionPointer(CallingConvention.Cdecl, CharSet = CharSet.Unicode, SetLastError = true)]
        private delegate IntPtr DDataBlockToMessage(
            [MarshalAs(UnmanagedType.U4)] uint size,
            [In] [MarshalAs(UnmanagedType.LPArray, SizeParamIndex = 0)] byte[] dataBlock);

        #endregion

        #region Public Methods and Operators

        /// <summary>
        /// </summary>
        /// <param name="inContext">
        /// </param>
        /// <param name="inChannelName">
        /// </param>
        public void Run(RemoteHooking.IContext inContext, string inChannelName)
        {
            try
            {
                dataBlockToMessageHolder = new DDataBlockToMessage(DataBlockToMessageHooked);
                sendHolder = new DSend(SendHooked);
                receiveMessageHook =
                    LocalHook.Create(
                        LocalHook.GetProcAddress("MessageProtocol.dll", "?DataBlockToMessage@@YAPAVMessage_t@@IPAX@Z"),
                        dataBlockToMessageHolder,
                        this);
                sendMessageHook = LocalHook.Create(
                    LocalHook.GetProcAddress("ws2_32.dll", "send"),
                    sendHolder,
                    this);
                receiveMessageHook.ThreadACL.SetExclusiveACL(new[] { 0 });
                sendMessageHook.ThreadACL.SetExclusiveACL(new[] { 0 });
            }
            catch (Exception e)
            {
                Interface.ReportException(e);
                return;
            }

            Interface.IsInstalled(RemoteHooking.GetCurrentProcessId());
            RemoteHooking.WakeUpProcess();

            try
            {
                while (true)
                {
                    Thread.Sleep(50);
                    GC.KeepAlive(dataBlockToMessageHolder);
                    DataClass[] package = null;
                    lock (incomingQueue)
                    {
                        if (incomingQueue.Count > 0)
                        {
                            package = incomingQueue.ToArray();
                            incomingQueue.Clear();
                        }
                    }

                    if (package != null)
                    {
                        foreach (var p in package)
                        {
                            p.ProcessId = RemoteHooking.GetCurrentProcessId();
                        }
                        Interface.IncomingMessage(package);
                    }

                    Interface.Ping();
                }
            }
            catch (Exception e)
            {
                Interface.ReportException(e);
            }
        }

        #endregion

        #region Methods

        /// <summary>
        /// </summary>
        /// <param name="size">
        /// </param>
        /// <param name="dataBlock">
        /// </param>
        /// <returns>
        /// </returns>
        [DllImport("MessageProtocol.dll", CharSet = CharSet.Unicode, SetLastError = true,
            CallingConvention = CallingConvention.Cdecl, EntryPoint = "?DataBlockToMessage@@YAPAVMessage_t@@IPAX@Z")]
        private static extern IntPtr DataBlockToMessage(
            [MarshalAs(UnmanagedType.U4)] uint size,
            [MarshalAs(UnmanagedType.LPArray)] byte[] dataBlock);

        /// <summary>
        /// </summary>
        /// <param name="size">
        /// </param>
        /// <param name="dataBlock">
        /// </param>
        /// <returns>
        /// </returns>
        private static IntPtr DataBlockToMessageHooked(
            [MarshalAs(UnmanagedType.U4)] uint size,
            [In] [MarshalAs(UnmanagedType.LPArray, SizeParamIndex = 0)] byte[] dataBlock)
        {
            lock (((Main)HookRuntimeInfo.Callback).incomingQueue)
            {
                ((Main)HookRuntimeInfo.Callback).incomingQueue.Push(new DataClass(dataBlock, true));
            }

            return DataBlockToMessage(size, dataBlock);
        }

        [DllImport("Ws2_32.dll", EntryPoint = "send", CallingConvention = CallingConvention.StdCall)]
        private static extern int send(
            [MarshalAs(UnmanagedType.U4)] uint s,
            [MarshalAs(UnmanagedType.LPArray, SizeParamIndex = 2)] byte[] buf,
            int len,
            int flags);

        private static int SendHooked(
            [MarshalAs(UnmanagedType.U4)] uint s,
            [In] [MarshalAs(UnmanagedType.LPArray, SizeParamIndex = 2)] byte[] buf,
            int len,
            int msgFlags)
        {
            try
            {
                lock (((Main)HookRuntimeInfo.Callback).incomingQueue)
                {
                    ((Main)HookRuntimeInfo.Callback).incomingQueue.Push(new DataClass(buf, false));
                }
            }
            catch (Exception e)
            {
                ((Main)HookRuntimeInfo.Callback).Interface.ReportException(e);
            }
            return send(s, buf, len, msgFlags);
        }

        #endregion
    }
}