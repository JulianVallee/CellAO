﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CellAO.Enums
{
    [Flags]
    public enum NanoFlags
    {
        // TODO: Collect the rest of the nano flags... I believe they fit into an Int32.. so there can only be 64 flags? (could be wrong)

        None = 0,                       // 00000000000000000000000000000000
        NoResistCannotFumble = 2,       // 00000000000000000000000000000010
        NotRemovable = 16384,           // 00000000000000000100000000000000
        IsHostile = 32768,              // 00000000000000001000000000000000
        IsBuff = 65536,                 // 00000000000000010000000000000000
        WantCollision = -2147483648     // 10000000000000000000000000000000
    }
}
