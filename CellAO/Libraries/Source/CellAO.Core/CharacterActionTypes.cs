﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CellAO.Core
{
    public enum CharacterActionTypes
    {
        LeftAttack,
        RightAttack,
        StopAttack,
        Despawn,
        Dispose,
        CastNano
    }
}
