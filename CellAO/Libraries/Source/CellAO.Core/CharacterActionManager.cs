﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CellAO.Core
{
    using CellAO.ObjectManager;
    using CellAO.Core.Entities;
    using CellAO.Enums;

    public sealed class CharacterActionManager : TimedContext
    {
        public CharacterActionManager(): base() {
            this.TIMER_DELAY = 200;
        }
        private static CharacterActionManager _instance = new CharacterActionManager();
        public static CharacterActionManager Instance { get { return _instance; } set { _instance = value; } }
        public override Dictionary<int, List<ITimedAction>> Queue
        {
            get
            {
                var dict = new Dictionary<int, List<ITimedAction>>();
                foreach(var c in Pool.Instance.GetAll<ICharacter>((int)IdentityType.Dynel))
                {
                    dict[c.Identity.Instance] = c.ActionsQueue;
                }
                return dict;
            }
            set
            {
                // Do nothing
            }
        }

        public static void Start()
        {
            _instance = _instance ?? new CharacterActionManager();
        }

    }
}
