﻿using System.Collections.Generic;
using CellAO.Core.Events;

namespace CellAO.Core.Nanos
{
    public interface INanoFormula
    {
        List<Event> Events { get; set; }

        int getItemAttribute(int number);
        bool isInstanced();
        int NanoStrain();
        int NCUCost();
        NanoFormula ShallowCopy();
    }
}