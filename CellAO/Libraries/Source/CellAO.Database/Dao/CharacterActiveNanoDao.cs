﻿
namespace CellAO.Database.Dao
{
    #region Usings ...

    using System;
    using System.Collections.Generic;
    using System.Data;
    using System.Linq;

    using CellAO.Database.Entities;

    using Dapper;

    using Utility;

    #endregion

    /// <summary>
    /// Data access object for LoginData
    /// </summary>
    public class CharacterActiveNanoDao : Dao<DBCharacterActiveNano>
    {
        private static CharacterActiveNanoDao _instance = null;
        public new static CharacterActiveNanoDao Instance
        {
            get
            {
                if (_instance == null)
                {
                    _instance = new CharacterActiveNanoDao();
                }
                return _instance;
            }
        }

        public IEnumerable<DBCharacterActiveNano> GetAllByCharacter(int charId)
        {
            return Instance.GetAll(new { Id = charId });
        }
        public IEnumerable<DBCharacterActiveNano> Save(IEnumerable<DBCharacterActiveNano> activeNanoFunctions,
            IDbConnection connection = null,
            IDbTransaction transaction = null)
        {
            foreach(var func in activeNanoFunctions)
            {
                Instance.Add(func, connection, transaction);
            }
            return activeNanoFunctions;
        }
    }
}