﻿#region License

// Copyright (c) 2005-2016, CellAO Team
// 
// 
// All rights reserved.
// 
// 
// Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:
// 
// 
//     * Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.
//     * Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in the documentation and/or other materials provided with the distribution.
//     * Neither the name of the CellAO Team nor the names of its contributors may be used to endorse or promote products derived from this software without specific prior written permission.
// 
// 
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
// "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
// LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
// A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR
// CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
// EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
// PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
// PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
// LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
// NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
// SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
// 

#endregion

namespace CellAO.Database.Entities
{

    /// <summary>
    /// </summary>
    [Table("VendorTemplate")]
    public class DBVendorTemplate : DBEntity
    {

        /// <summary>
        /// </summary>
        [PrimaryKey]
        [Column(nullable: false, autoIncrement: true, dbType: DBColumnType.UnsignedInt)]
        public override int Id { get; set; }


        /// <summary>
        /// </summary>
        [PrimaryKey]
        [Column(nullable: false, length: 7)]
        public string Hash { get; set; }

        /// <summary>
        /// </summary>
        [Column(nullable: false, defaultValue: 1)]
        public int Lvl { get; set; }

        /// <summary>
        /// </summary>
        [Column(nullable: false, defaultValue: "", length: 255)]
        public string Name { get; set; }

        /// <summary>
        /// </summary>
        [Column(nullable: false)]
        public int ItemTemplate { get; set; }

        /// <summary>
        /// </summary>
        [Column(nullable: false, length: 4)]
        public string ShopInvHash { get; set; }

        /// <summary>
        /// </summary>
        [Column(nullable: false, defaultValue: 1)]
        public int MinQl { get; set; }

        /// <summary>
        /// </summary>
        [Column(nullable: false, defaultValue: 1)]
        public int MaxQl { get; set; }

        /// <summary>
        /// </summary>
        [Column(nullable: false, float_M: 3, float_D: 2)]
        public float Buy { get; set; }

        /// <summary>
        /// </summary>
        [Column(nullable: false, float_M: 3, float_D: 2)]
        public float Sell { get; set; }

        /// <summary>
        /// </summary>
        [Column(nullable: false)]
        public int Skill { get; set; }

    }
}