﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="ContainerAddItemMessage.cs" company="SmokeLounge">
//   Copyright © 2013 SmokeLounge.
//   This program is free software. It comes without any warranty, to
//   the extent permitted by applicable law. You can redistribute it
//   and/or modify it under the terms of the Do What The Fuck You Want
//   To Public License, Version 2, as published by Sam Hocevar. See
//   http://www.wtfpl.net/ for more details.
// </copyright>
// <summary>
//   Defines the ContainerAddItemMessage type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace CellAO.Messages.N3Messages
{
    using CellAO.Interfaces;
    using CellAO.Interfaces.Attributes;
    using CellAO.Messages.Fields;

    [AoContract((int)N3MessageType.ContainerAddItem)]
    public class ContainerAddItemMessage : N3Message
    {
        #region Constructors and Destructors

        public ContainerAddItemMessage()
        {
            this.N3MessageType = N3MessageType.ContainerAddItem;
        }

        #endregion

        #region AoMember Properties

        [MessageField(0)]
        public Identity SourceContainer { get; set; }

        [MessageField(1)]
        public Identity Target { get; set; }

        [MessageField(2)]
        public int TargetPlacement { get; set; }

        #endregion
    }

    [AoContract((int)N3MessageType.ContainerAddItem2)]
    public class ContainerAddItem2Message : N3Message
    {
        #region Constructors and Destructors

        public ContainerAddItem2Message()
        {
            this.N3MessageType = N3MessageType.ContainerAddItem;
        }

        #endregion

        #region AoMember Properties

        [MessageField(0)]
        public Identity SourceContainer { get; set; }

        [MessageField(1)]
        public int TargetPlacement { get; set; }

        #endregion
    }
}