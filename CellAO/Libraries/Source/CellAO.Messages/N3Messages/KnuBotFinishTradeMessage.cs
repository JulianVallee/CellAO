﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="KnuBotFinishTradeMessage.cs" company="SmokeLounge">
//   Copyright © 2013 SmokeLounge.
//   This program is free software. It comes without any warranty, to
//   the extent permitted by applicable law. You can redistribute it
//   and/or modify it under the terms of the Do What The Fuck You Want
//   To Public License, Version 2, as published by Sam Hocevar. See
//   http://www.wtfpl.net/ for more details.
// </copyright>
// <summary>
//   Defines the KnuBotFinishTradeMessage type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace CellAO.Messages.N3Messages
{
    using CellAO.Interfaces;
    using CellAO.Interfaces.Attributes;

    [AoContract((int)N3MessageType.KnuBotFinishTrade)]
    public class KnuBotFinishTradeMessage : N3Message
    {
        #region Constructors and Destructors

        public KnuBotFinishTradeMessage()
        {
            this.N3MessageType = N3MessageType.KnuBotFinishTrade;
        }

        #endregion

        #region AoMember Properties

        [MessageField(0)]
        public short Unknown1 { get; set; }

        [MessageField(1)]
        public Identity Target { get; set; }

        [MessageField(2)]
        public int Decline { get; set; }

        #endregion
    }
}