// --------------------------------------------------------------------------------------------------------------------
// <copyright file="WeaponItemFullUpdateMessage.cs" company="SmokeLounge">
//   Copyright © 2013 SmokeLounge.
//   This program is free software. It comes without any warranty, to
//   the extent permitted by applicable law. You can redistribute it
//   and/or modify it under the terms of the Do What The Fuck You Want
//   To Public License, Version 2, as published by Sam Hocevar. See
//   http://www.wtfpl.net/ for more details.
// </copyright>
// <summary>
//   Defines the WeaponItemFullUpdateMessage type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace CellAO.Messages.N3Messages
{
    using CellAO.Enums;
    using CellAO.Enums.Serialization;
    using CellAO.Interfaces;
    using CellAO.Interfaces.Attributes;
    using CellAO.Messages.Fields;

    [AoContract((int)N3MessageType.WeaponItemFullUpdate)]
    public class WeaponItemFullUpdateMessage : N3Message
    {
        #region Constructors and Destructors

        public WeaponItemFullUpdateMessage()
        {
            this.N3MessageType = N3MessageType.WeaponItemFullUpdate;
            this.Unknown = 0x00;
            this.MsgVersion = 0x0000000B;

            this.Unknown4 = 0x01;
            this.SlotId = 0x06;
            /*
            this.StatsCount = 10090; //3F1 == 9
            this.FlagsKey = (int)StatIds.Flags;
            this.StaticInstanceKey = (int)StatIds.StaticInstance;  // 0x00000017
            this.ACGItemLevelKey = (int)StatIds.ACGItemLevel;
            this.ACGItemTemplateIDKey = (int)StatIds.ACGItemTemplateID;
            this.ACGItemTemplateID2Key = (int)StatIds.ACGItemTemplateID2;
            this.MultipleCountKey = (int)StatIds.MultipleCount;
            this.EnergyKey = (int)StatIds.Energy;*/
            //this.ItemDelayKey = 294; // atk delay
            //this.ItemRechargeKey = 210; // recharge delay,
        }

        public WeaponItemFullUpdateMessage(
                            Identity identity,
                            Identity characterIdentity,
                            Identity playfieldIdentity,
                            Identity relationalDBEntity,
                            byte slotId,
                            uint Flags,
                            uint StaticInstance,
                            uint ACGItemLevel,
                            uint ACGItemTemplateID,
                            uint ACGItemTemplateID2,
                            uint MultipleCount,
                            uint Energy,
                            uint ItemDelay,
                            uint ItemRecharge)
        {
            this.N3MessageType = N3MessageType.WeaponItemFullUpdate;
            this.Unknown = 0x00;
            this.MsgVersion = 0x0000000B;

            this.Unknown4 = 0x01;
            Identity = identity;
            CharacterOwner = characterIdentity;
            Playfield = playfieldIdentity.Instance;
            RelationalDBEntity = relationalDBEntity;
            SlotId = slotId;
            Stats = new GameTuple<StatIds, uint>[] {
                new GameTuple<StatIds, uint>() { Value1 = StatIds.flags, Value2 = Flags },
                new GameTuple<StatIds, uint>() { Value1 = StatIds.staticinstance, Value2 = StaticInstance },
                new GameTuple<StatIds, uint>() { Value1 = StatIds.ACGItemLevel, Value2 = ACGItemLevel },
                new GameTuple<StatIds, uint>() { Value1 = StatIds.ACGItemTemplateID, Value2 = ACGItemTemplateID },
                new GameTuple<StatIds, uint>() { Value1 = StatIds.ACGItemTemplateID2, Value2 = ACGItemTemplateID2 },
                new GameTuple<StatIds, uint>() { Value1 = StatIds.multiplecount, Value2 = MultipleCount },
                new GameTuple<StatIds, uint>() { Value1 = StatIds.energy, Value2 = Energy },
                new GameTuple<StatIds, uint>() { Value1 = StatIds.itemdelay, Value2 = ItemDelay },
                new GameTuple<StatIds, uint>() { Value1 = StatIds.rechargedelay, Value2 = ItemRecharge },
            };
            
        }

        #endregion

        #region AoMember Properties

        [MessageField(0)]
        public int MsgVersion { get; set; }

        [MessageField(1)]
        public Identity CharacterOwner { get; set; }

        [MessageField(2)]
        public int Playfield { get; set; }

        [MessageField(3)]
        public Identity RelationalDBEntity { get; set; }

        [MessageField(4)]
        public byte Unknown4 { get; set; }

        [MessageField(5)]
        public byte SlotId { get; set; }


        [MessageField(6, SerializeSize = ArraySizeType.X3F1)]
        public GameTuple<StatIds, uint>[] Stats { get; set; }
        /*
        [AoMember(6)]
        public int StatsCount { get; set; }

        [AoMember(7)]
        public int FlagsKey { get; set; }

        [AoMember(8)]
        public uint FlagsValue { get; set; }

        [AoMember(9)]
        public int StaticInstanceKey { get; set; }
        [AoMember(9)]
        public int StaticInstanceValue { get; set; }

        [AoMember(10)]
        public int ACGItemLevelKey { get; set; }

        [AoMember(11)]
        public int ACGItemLevelValue { get; set; }

        [AoMember(12)]
        public int ACGItemTemplateIDKey { get; set; }

        [AoMember(13)]
        public int ACGItemTemplateIDValue { get; set; }

        [AoMember(14)]
        public int ACGItemTemplateID2Key { get; set; }

        [AoMember(15)]
        public int ACGItemTemplateID2Value { get; set; }

        [AoMember(16)]
        public int MultipleCountKey { get; set; }

        [AoMember(17)]
        public int MultipleCountValue { get; set; }

        [AoMember(18)]
        public int EnergyKey { get; set; }

        [AoMember(19)]
        public uint EnergyValue { get; set; } */

        [MessageField(7)]
        public int UnknownLast4 { get; set; }

        #endregion
    }
}