﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CellAO.Messages.N3Messages
{
    using System.Dynamic;

    using CellAO.Messages.Fields;
    using CellAO.Interfaces.Attributes;
    using CellAO.Enums.Serialization;
    using CellAO.Interfaces;

    [AoContract((int)N3MessageType.QuestAlternative)]
    public class QuestAlternativeMessage:N3Message
    {
        public QuestAlternativeMessage()
        {
            this.N3MessageType = N3MessageType.QuestAlternative;
        }

        [MessageField(0)]
        public byte VersionId { get; set; } 

        [MessageField(1)]
        public byte LevelSlider { get; set; }
        [MessageField(2)]
        public byte GoodBadSlider { get; set; }
        [MessageField(3)]
        public byte OrderChaosSlider { get; set; }
        [MessageField(4)]
        public byte OpenHiddenSlider { get; set; }
        [MessageField(5)]
        public byte PhysicalMysticalSlider { get; set; }
        [MessageField(6)]
        public byte HeadOnStealthSlider { get; set; }
        [MessageField(7)]
        public byte MoneyExperienceSlider { get; set; }
        
        // Maybe this is the last Random which generated this mission packet
        [MessageField(8)]
        public int Unknown4 { get; set; }

        [MessageField(9)]
        public byte Unknown5 { get; set; }

        private Identity missionTerminalIdentity;

        public Identity GetMissionTerminalIdentity()
        {
            return missionTerminalIdentity;
        }

        public void SetMissionTerminalIdentity(Identity value)
        {
            missionTerminalIdentity = value;
        }

        [MessageField(11,SerializeSize = ArraySizeType.Byte)]
        public QuestInfo[] QuestInfos { get; set; }




    }
}
