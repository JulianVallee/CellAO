﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="PlayfieldAnarchyFMessage.cs" company="SmokeLounge">
//   Copyright © 2013 SmokeLounge.
//   This program is free software. It comes without any warranty, to
//   the extent permitted by applicable law. You can redistribute it
//   and/or modify it under the terms of the Do What The Fuck You Want
//   To Public License, Version 2, as published by Sam Hocevar. See
//   http://www.wtfpl.net/ for more details.
// </copyright>
// <summary>
//   Defines the PlayfieldAnarchyFMessage type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace CellAO.Messages.N3Messages
{
    using CellAO.Interfaces;
    using CellAO.Interfaces.Attributes;
    using CellAO.Messages.Fields;

    [AoContract((int)N3MessageType.PlayfieldAnarchyF)]
    public class PlayfieldAnarchyFMessage : N3Message
    {
        #region Constructors and Destructors

        public PlayfieldAnarchyFMessage()
        {
            this.N3MessageType = N3MessageType.PlayfieldAnarchyF;
            this.Unknown = 0x00;
            this.Unknown1 = 0x00000004;
            this.Unknown2 = 0x61;
        }

        #endregion

        #region AoMember Properties

        [MessageField(0)]
        public int Unknown1 { get; set; }

        [MessageField(1)]
        public Vector3 CharacterCoordinates { get; set; }

        [MessageField(2)]
        public byte Unknown2 { get; set; }

        [MessageField(3)]
        public Identity PlayfieldId1 { get; set; }

        [MessageField(4)]
        public int Unknown3 { get; set; }

        [MessageField(5)]
        public int Unknown4 { get; set; }

        [MessageField(6)]
        public Identity PlayfieldId2 { get; set; }

        [MessageField(7)]
        public int Unknown5 { get; set; }

        [MessageField(8)]
        public int Unknown6 { get; set; }

        [MessageField(9)]
        public PlayfieldVendorInfo PlayfieldVendorInfo { get; set; }

        [MessageField(10)]
        public int PlayfieldX { get; set; }

        [MessageField(11)]
        public int PlayfieldZ { get; set; }

        #endregion
    }
}