﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="TemplateActionMessage.cs" company="SmokeLounge">
//   Copyright © 2013 SmokeLounge.
//   This program is free software. It comes without any warranty, to
//   the extent permitted by applicable law. You can redistribute it
//   and/or modify it under the terms of the Do What The Fuck You Want
//   To Public License, Version 2, as published by Sam Hocevar. See
//   http://www.wtfpl.net/ for more details.
// </copyright>
// <summary>
//   Defines the TemplateActionMessage type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace CellAO.Messages.N3Messages
{
    using CellAO.Interfaces;
    using CellAO.Interfaces.Attributes;

    [AoContract((int)N3MessageType.TemplateAction)]
    public class TemplateActionMessage : N3Message
    {
        #region Constructors and Destructors

        public TemplateActionMessage()
        {
            this.N3MessageType = N3MessageType.TemplateAction;
        }

        #endregion

        #region AoMember Properties

        [MessageField(0)]
        public int ItemLowId { get; set; }

        [MessageField(1)]
        public int ItemHighId { get; set; }

        [MessageField(2)]
        public int Quality { get; set; }

        [MessageField(3)]
        public int Unknown1 { get; set; }

        [MessageField(4)]
        public int Unknown2 { get; set; }

        [MessageField(5)]
        public Identity Placement { get; set; }

        [MessageField(6)]
        public int Unknown3 { get; set; }

        [MessageField(7)]
        public int Unknown4 { get; set; }

        #endregion
    }
}