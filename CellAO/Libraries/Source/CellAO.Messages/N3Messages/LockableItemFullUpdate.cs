﻿using CellAO.Interfaces;
using CellAO.Interfaces.Attributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CellAO.Messages.N3Messages
{
    public class LockableItemFullUpdate : SimpleItemFullUpdateMessage
    {
        [MessageField(2)]
        public int LIFUVersion { get; set; }
        [MessageField(3)]
        public int LIFU_Unknown1 {get; set;}

        [MessageField(4)]
        public Identity[] Identites { get; set; }
    }
}
