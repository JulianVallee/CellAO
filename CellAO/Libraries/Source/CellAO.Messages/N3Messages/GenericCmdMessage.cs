﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="GenericCmdMessage.cs" company="SmokeLounge">
//   Copyright © 2013 SmokeLounge.
//   This program is free software. It comes without any warranty, to
//   the extent permitted by applicable law. You can redistribute it
//   and/or modify it under the terms of the Do What The Fuck You Want
//   To Public License, Version 2, as published by Sam Hocevar. See
//   http://www.wtfpl.net/ for more details.
// </copyright>
// <summary>
//   Defines the GenericCmdMessage type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace CellAO.Messages.N3Messages
{
    using CellAO.Interfaces;
    using CellAO.Interfaces.Attributes;
    using CellAO.Messages.Fields;

    [AoContract((int)N3MessageType.GenericCmd)]
    public class GenericCmdMessage : N3Message
    {
        #region Constructors and Destructors

        public GenericCmdMessage()
        {
            this.N3MessageType = N3MessageType.GenericCmd;
        }

        #endregion

        #region AoMember Properties

        [MessageField(0)]
        public int Temp1 { get; set; }

        [MessageField(1)]
        public int Count { get; set; }

        [MessageField(2)]
        public GenericCmdAction Action { get; set; }

        [MessageField(3)]
        public int Temp4 { get; set; }

        [MessageField(4)]
        public Identity User { get; set; }

        [MessageField(5)]
        public Identity[] Target { get; set; }

        #endregion
    }
}