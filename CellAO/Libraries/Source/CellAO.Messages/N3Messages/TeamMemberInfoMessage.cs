﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="TeamMemberInfoMessage.cs" company="SmokeLounge">
//   Copyright © 2013 SmokeLounge.
//   This program is free software. It comes without any warranty, to
//   the extent permitted by applicable law. You can redistribute it
//   and/or modify it under the terms of the Do What The Fuck You Want
//   To Public License, Version 2, as published by Sam Hocevar. See
//   http://www.wtfpl.net/ for more details.
// </copyright>
// <summary>
//   Defines the TeamMemberInfoMessage type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace CellAO.Messages.N3Messages
{
    using CellAO.Interfaces;
    using CellAO.Interfaces.Attributes;

    [AoContract((int)N3MessageType.TeamMemberInfo)]
    public class TeamMemberInfoMessage : N3Message
    {
        #region Constructors and Destructors

        public TeamMemberInfoMessage()
        {
            this.N3MessageType = N3MessageType.TeamMemberInfo;
            this.Unknown = 0x00;
            this.Unknown2 = 0x00000020;
            this.Unknown3 = 0x00000020;
            this.Unknown4 = 0x00000022;
            this.Unknown5 = 0x00000022;
        }

        #endregion

        #region AoMember Properties

        [MessageField(0)]
        public Identity Character { get; set; }

        [MessageField(1)]
        public int Unknown2 { get; set; }

        [MessageField(2)]
        public int Unknown3 { get; set; }

        [MessageField(3)]
        public int Unknown4 { get; set; }

        [MessageField(4)]
        public int Unknown5 { get; set; }

        #endregion
    }
}