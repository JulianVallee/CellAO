﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CellAO.Messages.Fields
{
    using CellAO.Interfaces.Attributes;

    public class FullCharacterSub
    {
        [MessageField(1)]
        public byte Unknown1 { get; set; }
        [MessageField(2)]
        public byte Unknown2 { get; set; }
        [MessageField(3)]
        public byte Unknown3 { get; set; }
    }
}
