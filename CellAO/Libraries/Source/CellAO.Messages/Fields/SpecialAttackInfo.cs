﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="SpecialAttackInfo.cs" company="SmokeLounge">
//   Copyright © 2013 SmokeLounge.
//   This program is free software. It comes without any warranty, to
//   the extent permitted by applicable law. You can redistribute it
//   and/or modify it under the terms of the Do What The Fuck You Want
//   To Public License, Version 2, as published by Sam Hocevar. See
//   http://www.wtfpl.net/ for more details.
// </copyright>
// <summary>
//   Defines the SpecialAttackInfo type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace CellAO.Messages.Fields
{
    using CellAO.Interfaces.Attributes;

    public class SpecialAttack
    {
        #region AoMember Properties

        [MessageField(0)]
        public int Unknown1 { get; set; }

        [MessageField(1)]
        public int Unknown2 { get; set; }

        [MessageField(2)]
        public int Unknown3 { get; set; }

        [MessageField(3, IsFixedSize = true, FixedSizeLength = 4)]
        public string Unknown4 { get; set; }

        #endregion
    }
}