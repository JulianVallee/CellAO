﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="NanoEffect.cs" company="SmokeLounge">
//   Copyright © 2013 SmokeLounge.
//   This program is free software. It comes without any warranty, to
//   the extent permitted by applicable law. You can redistribute it
//   and/or modify it under the terms of the Do What The Fuck You Want
//   To Public License, Version 2, as published by Sam Hocevar. See
//   http://www.wtfpl.net/ for more details.
// </copyright>
// <summary>
//   Defines the NanoEffect type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

using CellAO.Interfaces;
using CellAO.Interfaces.Attributes;

namespace CellAO.Messages.Fields
{

    public class NanoEffect
    {
        #region AoMember Properties

        [MessageField(0)]
        public Identity Effect { get; set; }

        [MessageField(1)]
        public int Unknown1 { get; set; }

        [MessageField(2)]
        public int CriterionCount { get; set; }

        [MessageField(3)]
        public int Hits { get; set; }

        [MessageField(4)]
        public int Delay { get; set; }

        [MessageField(5)]
        public int Unknown2 { get; set; }

        [MessageField(6)]
        public int Unknown3 { get; set; }

        [MessageField(7)]
        public int GfxValue { get; set; }

        [MessageField(8)]
        public int GfxLife { get; set; }

        [MessageField(9)]
        public int GfxSize { get; set; }

        [MessageField(10)]
        public int GfxRed { get; set; }

        [MessageField(11)]
        public int GfxGreen { get; set; }

        [MessageField(12)]
        public int GfxBlue { get; set; }

        [MessageField(13)]
        public int GfxFade { get; set; }

        #endregion
    }
}