﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CellAO.Messages.Fields
{
    using CellAO.Interfaces.Attributes;

    public class QuestItemShort
    {
        [MessageField(0)]
        public int LowId { get; set; }

        [MessageField(1)]
        public int HighId { get; set; }

        [MessageField(2)]
        public int Quality { get; set; }

        // Always 0?
        [MessageField(3)]
        public int Unknown1 { get; set; }
    }
}
