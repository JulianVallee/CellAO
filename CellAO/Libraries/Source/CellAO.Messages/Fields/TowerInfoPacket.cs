﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="TowerInfoPacket.cs" company="SmokeLounge">
//   Copyright © 2013 SmokeLounge.
//   This program is free software. It comes without any warranty, to
//   the extent permitted by applicable law. You can redistribute it
//   and/or modify it under the terms of the Do What The Fuck You Want
//   To Public License, Version 2, as published by Sam Hocevar. See
//   http://www.wtfpl.net/ for more details.
// </copyright>
// <summary>
//   Defines the TowerInfoPacket type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace CellAO.Messages.Fields
{
    using CellAO.Enums.Serialization;
    using CellAO.Interfaces.Attributes;
    using CellAO.Messages.N3Messages;

    public class TowerInfoPacket : InfoPacket
    {
        #region AoMember Properties

        [MessageField(0)]
        public byte Unknown1 { get; set; }

        [MessageField(1)]
        public byte Unknown2 { get; set; }

        [MessageField(2)]
        public byte Unknown3 { get; set; }

        [MessageField(3)]
        public byte Unknown4 { get; set; }

        [MessageField(4)]
        public byte Unknown5 { get; set; }

        [MessageField(5)]
        public byte Unknown6 { get; set; }

        [MessageField(6)]
        public byte Unknown7 { get; set; }

        [MessageField(7)]
        public int Health { get; set; }

        [MessageField(8)]
        public int MaxHealth { get; set; }

        [MessageField(9)]
        public int Unknown8 { get; set; }

        [MessageField(10)]
        public int OrganizationId { get; set; }

        [MessageField(11)]
        public short Unknown9 { get; set; }

        [MessageField(12)]
        public short Unknown10 { get; set; }

        [MessageField(13)]
        public short Unknown11 { get; set; }

        [MessageField(14, SerializeSize = ArraySizeType.Int16)]
        public byte[] FormattedText { get; set; }

        [MessageField(15)]
        public int TowerCount3F1 { get; set; }

        [MessageField(16)]
        public int TowerLowId { get; set; }

        [MessageField(17)]
        public int TowerHighId { get; set; }

        [MessageField(18)]
        public int TowerQuality { get; set; }

        [MessageField(19)]
        public int Unknown12 { get; set; }

        [MessageField(20)]
        [AoUsesFlags("flags", typeof(int), FlagsCriteria.EqualsToAny, new[] { (int)InfoPacketType.ControlTower })]
        public int? Timer { get; set; }

        [MessageField(21)]
        [AoUsesFlags("flags", typeof(byte), FlagsCriteria.EqualsToAny, new[] { (int)InfoPacketType.ControlTower })]
        public byte? Unknown13 { get; set; }

        [MessageField(22)]
        public int Unknown14 { get; set; }

        [MessageField(23)]
        public int Unknown15 { get; set; }

        [MessageField(24)]
        public int Unknown16 { get; set; }

        #endregion
    }
}