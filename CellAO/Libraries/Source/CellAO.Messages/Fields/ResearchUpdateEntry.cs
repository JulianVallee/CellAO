﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CellAO.Messages.Fields
{
    using CellAO.Interfaces.Attributes;

    public class ResearchUpdateEntry
    {
        [MessageField(1)]
        public int ResearchId { get; set; }
        [MessageField(2)]
        public int Unknown1 { get; set; }
        [MessageField(3)]
        public int Unknown2 { get; set; }
        [MessageField(4)]
        public int Unknown3 { get; set; }
    }
}
