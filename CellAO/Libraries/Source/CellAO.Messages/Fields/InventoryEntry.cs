﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CellAO.Messages.Fields
{
    using CellAO.Interfaces;
    using CellAO.Interfaces.Attributes;

    public class InventoryEntry
    {
        [MessageField(0)]
        public int Slotnumber { get; set; }
        [MessageField(1)]
        public short UnknownFlags { get; set; }
        [MessageField(2)]
        // Maybe stack count?
        public short Unknown1 { get; set; }
        [MessageField(3)]
        public Identity Identity { get; set; }
        [MessageField(4)]
        public int LowId { get; set; }
        [MessageField(5)]
        public int HighId { get; set; }
        [MessageField(6)]
        public int Quality { get; set; }
        [MessageField(7)]
        public int Unknown2 { get; set; }
    }
}
