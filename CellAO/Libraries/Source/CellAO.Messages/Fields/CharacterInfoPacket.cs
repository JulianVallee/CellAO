﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="CharacterInfoPacket.cs" company="SmokeLounge">
//   Copyright © 2013 SmokeLounge.
//   This program is free software. It comes without any warranty, to
//   the extent permitted by applicable law. You can redistribute it
//   and/or modify it under the terms of the Do What The Fuck You Want
//   To Public License, Version 2, as published by Sam Hocevar. See
//   http://www.wtfpl.net/ for more details.
// </copyright>
// <summary>
//   Defines the CharacterInfoPacket type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace CellAO.Messages.Fields
{
    using CellAO.Enums;
    using CellAO.Enums.Serialization;
    using CellAO.Interfaces.Attributes;

    using CellAO.Messages.N3Messages;

    public class CharacterInfoPacket : InfoPacket
    {
        #region AoMember Properties

        [MessageField(0)]
        public byte Unknown1 { get; set; }

        [MessageField(1)]
        [AoUsesFlags("flags", typeof(byte), FlagsCriteria.Default)]
        public Profession Profession { get; set; }

        [MessageField(2)]
        public byte Level { get; set; }

        [MessageField(3)]
        public byte TitleLevel { get; set; }

        [MessageField(4)]
        [AoUsesFlags("flags", typeof(byte), FlagsCriteria.Default)]
        public Profession VisualProfession { get; set; }

        [MessageField(5)]
        public short SideXp { get; set; }

        [MessageField(6)]
        public int Health { get; set; }

        [MessageField(7)]
        public int MaxHealth { get; set; }

        [MessageField(8)]
        public int BreedHostility { get; set; }

        [MessageField(9)]
        [AoUsesFlags("flags", typeof(int), FlagsCriteria.EqualsToAny, 
            new[]
                {
                    (int)InfoPacketType.CharacterOrg, (int)InfoPacketType.CharacterOrgSite, 
                    (int)InfoPacketType.CharacterOrgSiteTower
                })]
        public int? OrganizationId { get; set; }

        [MessageField(10, SerializeSize = ArraySizeType.Int16)]
        public string FirstName { get; set; }

        [MessageField(11, SerializeSize = ArraySizeType.Int16)]
        public string LastName { get; set; }

        [MessageField(12, SerializeSize = ArraySizeType.Int16)]
        public string LegacyTitle { get; set; }

        [MessageField(13)]
        public short Unknown2 { get; set; }

        [MessageField(14, SerializeSize = ArraySizeType.Int16)]
        [AoUsesFlags("flags", typeof(string), FlagsCriteria.EqualsToAny, 
            new[]
                {
                    (int)InfoPacketType.CharacterOrg, (int)InfoPacketType.CharacterOrgSite, 
                    (int)InfoPacketType.CharacterOrgSiteTower
                })]
        public string OrganizationRank { get; set; }

        [MessageField(15, SerializeSize = ArraySizeType.X3F1)]
        [AoUsesFlags("flags", typeof(TowerField[]), FlagsCriteria.EqualsToAny, 
            new[] { (int)InfoPacketType.CharacterOrgSite, (int)InfoPacketType.CharacterOrgSiteTower })]
        public TowerField[] TowerFields { get; set; }

        [MessageField(16)]
        public int CityPlayfieldId { get; set; }

        [MessageField(17, SerializeSize = ArraySizeType.X3F1)]
        [AoUsesFlags("flags", typeof(Tower[]), FlagsCriteria.EqualsToAny, 
            new[] { (int)InfoPacketType.CharacterOrgSiteTower })]
        public Tower[] Towers { get; set; }

        [MessageField(18)]
        public int InvadersKilled { get; set; }

        [MessageField(19)]
        public int KilledByInvaders { get; set; }

        [MessageField(20)]
        public int AiLevel { get; set; }

        [MessageField(21)]
        public int PvpDuelWins { get; set; }

        [MessageField(22)]
        public int PvpDuelLoses { get; set; }

        [MessageField(23)]
        public int PvpProfessionDuelLoses { get; set; }

        [MessageField(24)]
        public int PvpSoloKills { get; set; }

        [MessageField(25)]
        public int PvpTeamKills { get; set; }

        [MessageField(26)]
        public int PvpSoloScore { get; set; }

        [MessageField(27)]
        public int PvpTeamScore { get; set; }

        [MessageField(28)]
        public int PvpDuelScore { get; set; }

        #endregion
    }
}