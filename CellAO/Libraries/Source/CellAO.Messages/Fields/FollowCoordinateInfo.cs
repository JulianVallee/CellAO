﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CellAO.Messages.Fields
{
    using CellAO.Interfaces.Attributes;

    public class FollowCoordinateInfo : FollowInfo
    {
        private byte followInfoType = 1;
        [MessageField(0)]
        public byte FollowInfoType
        {
            get
            {
                return this.followInfoType;
            }
            set
            {
                this.followInfoType = value;
            }
        }
        [MessageField(1)]
        public byte MoveMode { get; set; }

        [MessageField(2)]
        public byte CoordinateCount { get; set; }

        [MessageField(3)]
        public Vector3 CurrentCoordinates { get; set; }

        [MessageField(4)]
        public Vector3 EndCoordinates { get; set; }
    }
}
