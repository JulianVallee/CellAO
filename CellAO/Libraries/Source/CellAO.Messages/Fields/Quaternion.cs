﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="Quaternion.cs" company="SmokeLounge">
//   Copyright © 2013 SmokeLounge.
//   This program is free software. It comes without any warranty, to
//   the extent permitted by applicable law. You can redistribute it
//   and/or modify it under the terms of the Do What The Fuck You Want
//   To Public License, Version 2, as published by Sam Hocevar. See
//   http://www.wtfpl.net/ for more details.
// </copyright>
// <summary>
//   Defines the Quaternion type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace CellAO.Messages.Fields
{
    using CellAO.Interfaces.Attributes;

    public class Quaternion
    {
        #region AoMember Properties

        [MessageField(0)]
        public float X { get; set; }

        [MessageField(1)]
        public float Y { get; set; }

        [MessageField(2)]
        public float Z { get; set; }

        [MessageField(3)]
        public float W { get; set; }

        #endregion
    }
}